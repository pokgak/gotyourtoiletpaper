@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h2>Edit Item</h2>

            <div class="card">
                <div class="card-header">Add new Item</div>
                <div class="card-body">
                    TODO: create new
                </div>
            </div>

            <div class="card">
                <div class="card-header">Update Item</div>
                <div class="card-body">
                    TODO: update existing
                </div>
            </div>

            <div class="card">
                <div class="card-header">Delete Item</div>
                <div class="card-body">
                    TODO: delete
                </div>
            </div>
        </div>
    </div>
</div>
@endsection